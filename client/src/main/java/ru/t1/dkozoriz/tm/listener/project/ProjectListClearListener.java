package ru.t1.dkozoriz.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectListClearRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Component
public final class ProjectListClearListener extends AbstractProjectListener {

    public ProjectListClearListener() {
        super("project-clear", "delete all projects.");
    }

    @Override
    @EventListener(condition = "@projectListClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.projectListClear(new ProjectListClearRequest(getToken()));
    }

}