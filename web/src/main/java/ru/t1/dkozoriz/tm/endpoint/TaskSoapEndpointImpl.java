package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkozoriz.tm.dto.soap.*;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;


@Endpoint
@RequiredArgsConstructor
public class TaskSoapEndpointImpl {

    private final TaskService taskService;

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.dkozoriz.t1.ru/dto/soap";

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.deleteAll();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        final TaskCountResponse response = new TaskCountResponse();
        response.setCountTask(taskService.count());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    private TaskCreateResponse create(@RequestPayload final TaskCreateRequest request) {
        final TaskCreateResponse response = new TaskCreateResponse();
        final Task task = new Task(request.getName());
        taskService.save(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        final TaskFindByIdResponse response = new TaskFindByIdResponse();
        final Task task = taskService.findById(request.getId());
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    private TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.update(request.getTask());
        return new TaskSaveResponse();
    }

}