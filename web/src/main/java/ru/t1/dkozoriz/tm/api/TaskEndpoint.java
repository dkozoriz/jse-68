package ru.t1.dkozoriz.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

    @GetMapping("/getAll")
    @WebMethod
    List<Task> getAll();

    @GetMapping("/count")
    @WebMethod
    Long count();

    @GetMapping("/get/{id}")
    @WebMethod
    Task get(
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/post")
    Task post(@RequestBody Task task);

    @WebMethod
    @PutMapping("/put")
    Task put(
            @RequestBody Task task
    );

    @DeleteMapping("/delete/{id}")
    @WebMethod
    void delete(
            @PathVariable("id") String id
    );

    @DeleteMapping("/deleteAll")
    @WebMethod
    void deleteAll();

}