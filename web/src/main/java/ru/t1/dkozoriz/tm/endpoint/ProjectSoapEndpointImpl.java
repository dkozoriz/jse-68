package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkozoriz.tm.dto.soap.*;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;


@Endpoint
@RequiredArgsConstructor
public class ProjectSoapEndpointImpl {

    private final ProjectService projectService;

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.dkozoriz.t1.ru/dto/soap";

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.deleteAll();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        final ProjectCountResponse response = new ProjectCountResponse();
        response.setCountProject(projectService.count());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    private ProjectCreateResponse create(@RequestPayload final ProjectCreateRequest request) {
        final ProjectCreateResponse response = new ProjectCreateResponse();
        final Project project = new Project(request.getName());
        projectService.save(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        final Project project = projectService.findById(request.getId());
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    private ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.update(request.getProject());
        return new ProjectSaveResponse();
    }

}